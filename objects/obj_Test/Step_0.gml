/// @description Check inputs

var device = 0;

if(inputs_check(KEY_RESTART, device)) {
	room_restart();
}

if(inputs_check(KEY_EXIT, device)) {
	game_end();
}