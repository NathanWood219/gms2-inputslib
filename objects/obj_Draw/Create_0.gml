/// @description Variables

var red = irandom(255);
var green = irandom(255);
var blue = irandom(255);

COLOR	= make_color_rgb(red, green, blue);