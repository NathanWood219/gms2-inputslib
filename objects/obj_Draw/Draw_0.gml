/// @description Draw random background color

draw_set_color(COLOR);
draw_rectangle(0, 0, room_width, room_height, false);