///inputs_map()
// Call ONCE at game init

global.key_map = ds_map_create();

// INPUT CONSTANTS
//	uses "ASCII+" for additional inputs
// ---------------------------------------------
//		   Map				Constant name		Char
ds_map_add(global.key_map,	"mb_wheel_up",		256);
ds_map_add(global.key_map,	"mb_wheel_down",	257);

ds_map_add(global.key_map,	"gp_axisl_left",	300);
ds_map_add(global.key_map,	"gp_axisl_right",	301);
ds_map_add(global.key_map,	"gp_axisl_up",		302);
ds_map_add(global.key_map,	"gp_axisl_down",	303);
ds_map_add(global.key_map,	"gp_axisr_left",	304);
ds_map_add(global.key_map,	"gp_axisr_right",	305);
ds_map_add(global.key_map,	"gp_axisr_up",		306);
ds_map_add(global.key_map,	"gp_axisr_down",	307);