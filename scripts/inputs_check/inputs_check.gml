///inputs_check(keybinding, [gp_device])

// This script returns the exact state of the specificed binding
// To check input pressed/released, add boolean states for switching
// Then when released, check the specific state again
// Okay I can just add this I guess

// ---------------------------------------------------- //
//	KEYBINDING CONSTANTS
// ---------------------------------------------------- //

// ---------------------------------------------------- //
//			MOUSE WHEEL
// ---------------------------------------------------- //

var M_WHEEL_UP			= 256;							// ASCII+, range: (255, ?)
var M_WHEEL_DOWN		= 257;							// .


// ---------------------------------------------------- //
//			KEYBOARD
// ---------------------------------------------------- //

//			Place extra keyboard ASCII+ chars here

// ---------------------------------------------------- //
//			GAMEPAD
// ---------------------------------------------------- //

// NOTE: Gamepad requires a device

var GP_AXIS_MIN			= 0.5;							// Inclusive, range: [0, max]
var GP_AXIS_MAX			= 1;							// Inclusive, range: [min, 1]

var GP_AXISL_LEFT		= 300;							// ASCII+, range (255, ?)
var GP_AXISL_RIGHT		= 301;							// .
var GP_AXISL_UP			= 302;							// .
var GP_AXISL_DOWN		= 303;							// .
var GP_AXISR_LEFT		= 304;							// .
var GP_AXISR_RIGHT		= 305;							// .
var GP_AXISR_UP			= 306;							// .
var GP_AXISR_DOWN		= 307;							// .


// ---------------------------------------------------- //
//			CUSTOM
// ---------------------------------------------------- //

//			Place custom input ASCII+ chars here

// ---------------------------------------------------- //
//	RETURN BASED ON INPUT
// ---------------------------------------------------- //

var input = argument[0];
	
switch(input) {

	// MOUSE BUTTONS
	case mb_left:
	case mb_middle:
	case mb_right:
	case mb_none:
	case mb_any:
		return mouse_check_button(input);
	
	// MOUSE WHEEL
	case M_WHEEL_UP:
		return mouse_wheel_up();
	case M_WHEEL_DOWN:
		return mouse_wheel_down();
	
	// GAMEPAD BUTTONS
	case gp_face1:
	case gp_face2:
	case gp_face3:
	case gp_face4:
	case gp_shoulderl:
	case gp_shoulderlb:
	case gp_shoulderr:
	case gp_shoulderrb:
	case gp_select:
	case gp_start:
	case gp_stickl:
	case gp_stickr:
	case gp_padu:
	case gp_padd:
	case gp_padl:
	case gp_padr:
		return gamepad_button_check(argument[1], input);
	
	// GAMEPAD AXIS
	case GP_AXISL_LEFT:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axislh);
		return (abs(value) >= GP_AXIS_MIN && abs(value) <= GP_AXIS_MAX);
	case GP_AXISL_RIGHT:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axislh);
		return (value >= GP_AXIS_MIN && value <= GP_AXIS_MAX);
	case GP_AXISL_UP:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axislv);
		return (abs(value) >= GP_AXIS_MIN && abs(value) <= GP_AXIS_MAX);
	case GP_AXISL_DOWN:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axislv);
		return (value >= GP_AXIS_MIN && value <= GP_AXIS_MAX);

	case GP_AXISR_LEFT:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axisrh);
		return (abs(value) >= GP_AXIS_MIN && abs(value) <= GP_AXIS_MAX);
	case GP_AXISR_RIGHT:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axisrh);
		return (value >= GP_AXIS_MIN && value <= GP_AXIS_MAX);
	case GP_AXISR_UP:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axisrv);
		return (abs(value) >= GP_AXIS_MIN && abs(value) <= GP_AXIS_MAX);
	case GP_AXISR_DOWN:
		var gp_device	= argument[1];
		var value		= gamepad_axis_value(gp_device, gp_axisrv);
		return (value >= GP_AXIS_MIN && value <= GP_AXIS_MAX);
	default:
		break;
}


// KEYBOARD LAST
return keyboard_check(input);